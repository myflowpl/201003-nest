import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

export class CommentModel {
  @ApiPropertyOptional()
  id: number;
  @ApiProperty({
    description: 'Imię komentującego',
    example: 'Piotr'
  })
  name: string;
}
