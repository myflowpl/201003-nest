import { Body, Controller, Get, HttpException, HttpStatus, Post, Request, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiCreatedResponse } from '@nestjs/swagger';
import { Roles } from '../decorators/roles.decorator';
import { User } from '../decorators/user.decorator';
import { UserRegisterResponseDto, UserRegisterRequestDto, UserLoginRequestDto, UserLoginResponseDto } from '../dto';
import { AuthGuard } from '../guards/auth.guard';
import { UserRole } from '../models';
import { AuthService } from '../services/auth.service';
import { UserService } from '../services/user.service';

@Controller('user')
export class UserController {

  constructor(
    private userService: UserService,
    private authService: AuthService,
  ) {}

  @Post('login')
  async login(@Body() credentials: UserLoginRequestDto): Promise<UserLoginResponseDto> {

    const user = await this.userService.findByCredentials(credentials.email, credentials.password);

    if (!user) {
      throw new HttpException('ValidationError', HttpStatus.UNPROCESSABLE_ENTITY);
    }
    return {
      token: await this.authService.tokenSign({user}),
      user,
    };
  }


  @Post('register')
  @ApiCreatedResponse({type: UserRegisterResponseDto})
  async register(@Body() data: UserRegisterRequestDto): Promise<UserRegisterResponseDto> {
    const user = await this.userService.create(data);
    // TODO handle errors
    return {
      user,
    };
  }
  
  @Get()
  @Roles(UserRole.ADMIN)
  @UseGuards(AuthGuard)
  @ApiBearerAuth()
  getUser(@User() user) {
    return {user};
  }

}

