import { ApiProperty } from '@nestjs/swagger';
import { UserModel } from '../models';

export class UserRegisterRequestDto {
  name: string;
  email: string;
  password: string;
}

export class UserRegisterResponseDto {
  user: UserModel;
}

export class UserLoginRequestDto {
  @ApiProperty({
    example: 'piotr@myflow.pl'
  })
  email: string;
  @ApiProperty({
    example: '123'
  })
  password: string;
}

export class UserLoginResponseDto {
  token: string;
  user: UserModel;
}
